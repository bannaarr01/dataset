<!-- ======= Ready Section ======= -->
<section id="ready">
    <div class="container">

        <div class="container-fluid">
            <div class="row">

                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <div class="d-flex justify-content-between px-md-1">
                        <div class="align-self-center">
                            Looking to post a job?
                            <a id="signbt" class="btn btn-lg text-white" href="{{ route('employer.register') }}">Sign
                                up</a>
                        </div>
                    </div>
                </div>


                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <div class="d-flex justify-content-between px-md-1">
                        <div class="align-self-center">
                            Looking to find a job?
                            <a id="signsecbt" class="btn btn-lg text-white" href="{{ route('register') }}">Sign up
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section> <!-- ======= End of Ready Section ======= -->
