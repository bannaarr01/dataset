<!-- ======= Steps Section ======= -->
<section id="steps" class="steps">
    <div class="container" data-aos="zoom-in">
        <div class="card col-xl-4 col-md-4 col-12 mb-4 ml-auto mx-auto">
            <div class="card-header">
                <h2 id="apply" class="text-center">APPLY PROCESS</h2>
                <h2 id="how" class="text-center mb-3">How it works</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-4 col-md-6 col-12 mb-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between px-md-1">
                            <div>
                                <h3 class="text-white">1. Search a job</h3>
                                <p class="mb-0 text-white">You can search either by keywords, job title and sort by any
                                    preference you want</p>
                            </div>
                            <div class="align-self-center">
                                <i class="fas fa-search text-danger fa-3x"></i>
                            </div>
                        </div>
                        <div class="px-md-1">
                            <div class="progress mt-3 mb-1 rounded" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 25%;"
                                     aria-valuenow="25"
                                     aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12 mb-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between px-md-1">
                            <div>
                                <h3 class="text-white">2. Apply for job</h3>
                                <p class="mb-0 text-white">After Successful registration or login, then you can apply
                                    for the job searched</p>
                            </div>
                            <div class="align-self-center">
                                <i class="fas fa-user-check text-warning fa-3x"></i>
                            </div>
                        </div>
                        <div class="px-md-1">
                            <div class="progress mt-3 mb-1 rounded" style="height: 7px;">
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 75%;"
                                     aria-valuenow="75"
                                     aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12 mb-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between px-md-1">
                            <div>
                                <h3 class="text-white">3. Get your job</h3>
                                <p class="mb-0 text-white">Once you have applied, employer checks your application and
                                    sent you feedbacks</p>
                            </div>
                            <div class="align-self-center">
                                <i class="fas fa-user-tie text-success fa-3x"></i>
                            </div>
                        </div>
                        <div class="px-md-1">
                            <div class="progress mt-3 mb-1 rounded" style="height: 7px;">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 100%;"
                                     aria-valuenow="100"
                                     aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- Steps Section -->
