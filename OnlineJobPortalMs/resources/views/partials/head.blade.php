<!-- Favicons -->
<link href="{{ asset('template/assets/img/favicon.ico') }}" rel="icon">
<link href="{{ asset('template/assets/img/apple-icon.png') }}" rel="apple-touch-icon">


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
    href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,300;1,400;1,600;1,700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
    rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="{{ asset('template/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/assets/vendor/venobox/venobox.css" rel="stylesheet') }}">
<link href="{{ asset('template/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
<link href="{{ asset('template/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/assets/vendor/aos/aos.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- Template Main CSS File -->
<link href="{{ asset('template/assets/css/style.css') }}" rel="stylesheet">


